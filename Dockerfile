# First Stage
FROM node:alpine as build

WORKDIR /build

ADD . .

RUN yarn install
RUN yarn build

# Second stage
FROM nginx:stable-alpine

WORKDIR /app

COPY --from=build /build/dist /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]