import Vue from 'vue'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faGithub,
  faLinkedin,
  faGoogleDrive,
  faGoogle,
  faDev
} from '@fortawesome/free-brands-svg-icons'

library.add(faGithub)
library.add(faLinkedin)
library.add(faGoogleDrive)
library.add(faGoogle)
library.add(faDev)

Vue.component('FontAwesome', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
